# vue-cli3-template

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
#### 介绍
vue-cli3 + element搭建的基本项目模板

#### 软件架构
软件架构说明


#### 安装教程

1.  npm install
2.  npm run serve
3.  npm run build

#### 使用说明

1.  vue.config.js配置、开发生产配置
2.  axios封装
3.  sass屏幕适配 r(16)
4.  echarts、element、js-cookie、crypto-js、jsencrypt(加解密)、nprogress(路由进度条)
5.  formCheck(表单校验)、desensitization(名字脱敏)、stopBtnRepeat(阻止按钮重复提交)

#### 参与贡献

#### 特技

欢迎来访作者博客 [https://www.cnblogs.com/chichuan/]更多精彩尽在这里
