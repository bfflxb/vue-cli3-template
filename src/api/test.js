import request from '@/utils/request'

// 测试接口
export function getTest(query) {
  return request({
    url: '/test',
    method: 'get',
    params: query
  })
}
// 测试登录过期接口
export function getInvalid(query) {
  return request({
    url: '/invalid',
    method: 'get',
    params: query
  })
}