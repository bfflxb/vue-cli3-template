import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import echarts from 'echarts'
import {
  desensitization
} from "@/utils/desensitization.js";
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './styles/reset.css';
import './permission'
import jsCookie from 'js-cookie'
import rules from './utils/formCheck.js'
import './utils/stopBtnRepeat.js'

Vue.use(Element, {
  size: 'small',
  zIndex: 3000
});

Vue.config.productionTip = false
Vue.prototype.$echarts = echarts
Vue.prototype.$desensitization = desensitization
Vue.prototype.$cookie = jsCookie;
Vue.prototype.$rules = rules

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')