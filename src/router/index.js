import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: '/',
    redirect: 'home',
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: {
      isLogin: true
    }
  },
  {
    path: '/test',
    name: 'test',
    component: () => import(/* webpackChunkName: "about" */ '../views/Test.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  }
]

const router = new VueRouter({
  routes,
  // base: 'bff', // nginx配置
  mode: 'history', // 去掉url中的#
})

export default router
