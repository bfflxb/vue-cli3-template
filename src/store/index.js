import Vue from 'vue'
import Vuex from 'vuex'
// import state from './state'
// import mutations from './multation'
// import actions from './actions'
import getters from './getters'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  // state,
  // mutations,
  // actions,
  modules: {
    user
  },
  getters
})