import $ from 'jquery';
import Vue from 'vue';

let isSidebar = false;

const cardDrag = {
  bind(el) {
    bindContent(el.querySelector('.move_header'), el, true);
    bindContent(el.querySelector('.move_btn'), el, false);
    // console.log('dialogHeaderEl',dialogHeaderEl)
    // dialogHeaderEl.style.cursor = 'move';
  }
};

function bindContent(dialogHeaderEl, el, isSideBar) {
  if (!dialogHeaderEl) {
    return;
  }
  dialogHeaderEl.style.cssText += ';cursor:move;';

  let startTime = '';
  let lastTime = '';
  dialogHeaderEl.onmousedown = (e) => {
    // const dragDom = dialogHeaderEl.parentNode.parentNode;
    startTime = new Date().getTime();
    const dragDom = el;
    let domOrigin = getPosition(dragDom);
    // 鼠标按下，计算当前元素距离可视区的距离
    const disX = e.clientX;
    const disY = e.clientY;

    const screenWidth = domOrigin.parentW; // body当前宽度
    const screenHeight =
      domOrigin.parentH === 0 ? Number.MAX_VALUE : domOrigin.parentH; // 可见区域高度(应为body高度，可某些环境下无法获取,但是绝对不能为0,获取不到时不进行下停靠)

    const dragDomWidth = el.offsetWidth; // 对话框宽度
    let dragDomHeight = el.offsetHeight; // 对话框高度
    if (!dragDomHeight) {
      dragDomHeight = el.lastElementChild.offsetHeight;
    }
    el.style.setProperty('--dragDomHeight', dragDomHeight + 'px');

    let interDis = 30; // 进入屏幕的触发停靠的距离
    let topInterDis = 10; // top停靠特殊 数值大了手感不好
    const minDragDomLeft = 0 - interDis;
    const maxDragDomLeft = screenWidth - dragDomWidth + interDis;

    const minDragDomTop = 0 - topInterDis;
    // const maxDragDomTop = screenHeight - dragDomHeight + interDis;

    const minDragBtnLeft = 0;
    const maxDragBtnLeft = screenWidth - dialogHeaderEl.offsetWidth;

    const minDragBtnTop = 0;
    const maxDragBtnTop = screenHeight - dialogHeaderEl.offsetHeight;

    // 获取到的值带px 正则匹配替换
    /* let styL = sty(dragDom, 'left');
    let styT = sty(dragDom, 'top');

    // 注意在ie中 第一次获取到的值为组件自带50% 移动之后赋值为px
    if (styL.includes('%')) {
        styL = +document.body.clientWidth * (+styL.replace(/\%/g, '') / 100);
        styT = +document.body.clientHeight * (+styT.replace(/\%/g, '') / 100);
    } else {
        styL = +styL.replace(/\px/g, '');
        styT = +styT.replace(/\px/g, '');
    }*/

    let styL = domOrigin.x;
    let styT = domOrigin.y;

    document.onmousemove = function (e) {
      if (el.getAttribute('isFix') === 'true') {
        return;
      }
      // const dragDom = dialogHeaderEl.parentNode.parentNode;
      const dragDom = el;
      // console.log('dragDom', dragDom)
      // 通过事件委托，计算移动的距离
      let left = e.clientX - disX;
      let top = e.clientY - disY;

      let trueLeft = left + styL;
      let trueTop = top + styT;
      if (!isSideBar) {
        if (trueLeft < minDragBtnLeft) {
          return;
          // 右停靠
        } else if (trueLeft > maxDragBtnLeft) {
          return;
        }
        if (trueTop < minDragBtnTop) {
          return;
        } else if (trueTop > maxDragBtnTop) {
          return;
        }
      }
      let minBottom = 32; // 离地最小高度,默认标题栏的高度
      let expectBottom =
        screenHeight - trueTop > minBottom ? screenHeight - trueTop : minBottom;
      let horizontalPosition =
        trueLeft > screenWidth / 2
          ? `right:${screenWidth - dragDomWidth - trueLeft}px;left:unset`
          : `left:${trueLeft}px;right:unset`;
      let verticalPosition =
        trueTop > screenHeight / 2
          ? `bottom:calc(${expectBottom}px - var(--dragDomHeight));top:unset`
          : `top:${trueTop}px;bottom:unset`;
      // 移动当前元素
      dragDom.style.cssText += `position:absolute;${horizontalPosition};${verticalPosition}`;

      // 停靠
      if (isSideBar) {
        // 左停靠
        if (trueLeft < minDragDomLeft) {
          sidebarDom(dragDom, dragDomWidth, 'left');
          // 右停靠
        } else if (trueLeft > maxDragDomLeft) {
          sidebarDom(dragDom, dragDomWidth, 'right');
        }
        if (trueTop < minDragDomTop) {
          sidebarDom(dragDom, dragDomHeight, 'top');
        } else {
          if (!isSidebar) {
            dragDom.style.transition = '';
            $(dragDom).unbind('mouseenter').unbind('mouseleave');
          }
        }
      }
    };

    document.onmouseup = function () {
      lastTime = new Date().getTime();
      if (lastTime - startTime > 200) {
        dialogHeaderEl.setAttribute('isClick', false);
      } else {
        dialogHeaderEl.setAttribute('isClick', true);
      }
      // console.log(dialogHeaderEl.getAttribute('isClick'))
      document.onmousemove = null;
      document.onmouseup = null;
      window.getSelection
        ? window.getSelection().removeAllRanges()
        : document.selection.empty();
      return false;
    };
  };
}

// 获取原有属性 ie dom元素.currentStyle 火狐谷歌 window.getComputedStyle(dom元素, null);
/* const sty = (function() {
  if (window.document.currentStyle) {
    return (dom, attr) => dom.currentStyle[attr];
  } else {
    return (dom, attr) => getComputedStyle(dom, false)[attr];
  }
})();*/

const getPosition = function (el) {
  let _x = 0;
  let _y = 0;
  do {
    _x += el.offsetLeft;
    _y += el.offsetTop;
    el = el.offsetParent;
  } while (el !== null && window.getComputedStyle(el).position !== 'relative'); // transform!=none停止 transform影响fixed  && window.getComputedStyle(el).transform === 'none'
  return {
    x: _x,
    y: _y,
    parentW: el ? el.offsetWidth : document.body.clientWidth,
    parentH: el ? el.offsetHeight : document.body.clientHeight
  };
};

function sidebarDom(dragDom, dragDomWidth, direction) {
  document.onmousemove = null;
  isSidebar = true;
  if (direction === 'right') {
    $(dragDom).css('left', '');
  }
  if (direction === 'bottom') {
    $(dragDom).css('top', '');
  }
  let tagDirection = direction === 'right' || direction === 'left' ? 'V' : 'H';

  dragDom.style.cssText += `position:absolute;${direction}:${
    -dragDomWidth + 10
  }px;opacity:0;transition: all 0.7s;`;

  let tagDom = dragDom.parentElement.getElementsByClassName(
    'tag' + tagDirection
  )[0];
  tagDom.style.display = 'block';
  tagDom.style.position = 'absolute';
  // 贴边左右时，只需修改动态top(left or right永远是0),  贴边上时，只需修改动态left(top永远是0)
  let positionName;
  let value;
  if (tagDirection === 'V') {
    positionName = dragDom.style['top'] === 'unset' ? 'bottom' : 'top';
    value = Number(dragDom.style[positionName].replace('px', ''));
    // bottom 重新计算bottom 否则标签会在容器底部
    if (positionName === 'bottom') {
      value = dragDom.style[positionName].replace(
        'var(--dragDomHeight)',
        tagDom.offsetHeight + 'px'
      );
    }
  } else {
    positionName = dragDom.style['left'] === 'unset' ? 'right' : 'left';
    value = Number(dragDom.style[positionName].replace('px', ''));
    // right 重新计算right 否则标签会在容器右部
    if (positionName === 'bottom') {
      value = value + dragDom.offsetWidth - tagDom.offsetWidth;
    }
  }
  tagDom.style[positionName] = typeof value === 'number' ? value + 'px' : value;
  tagDom.style[direction] = '0px';

  let tagInfoName = direction + 'TagInfo';
  // 获取其他组件的Tag位置信息
  if (Vue.prototype[tagInfoName]) {
    for (let tagInfo of Vue.prototype[tagInfoName]) {
      if (tagInfo.id !== tagDom.id) {
        // eslint-disable-next-line no-empty
        if (tagDom.style[positionName] > tagInfo.position) {
        }
      }
    }
  }
  // 保存组件的Tag位置信息
  if (!Vue.prototype[tagInfoName]) {
    Vue.prototype[tagInfoName] = [];
  }
  Vue.prototype[tagInfoName].push({
    position: tagDom.style[positionName],
    height: tagDom.offsetHeight,
    tag: tagDom,
    id: tagDom.id
  });

  tagDom.onclick = function () {
    $(dragDom).css(direction, '0px');
    $(dragDom).css('opacity', '1');
    $(tagDom).css('display', 'none');
    tagDom.style.top = '';
    tagDom.style.left = '';
    tagDom.style.right = '';
    tagDom.style.bottom = '';
    tagDom.onclick = '';
  };
  setTimeout(() => {
    isSidebar = false;
  }, 500);
}

export default cardDrag;
