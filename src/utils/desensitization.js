// 名字脱敏
export function desensitization(str, frontLen, endLen) {
  var len = str.length - frontLen - endLen;
  var xing = "";
  for (var i = 0; i < len; i++) {
    xing += " * ";
  }
  return (
    str.substring(0, frontLen) + xing + str.substring(str.length - endLen)
  );
}