/* eslint-disable no-useless-escape */
// 手机号码验证
const validatePhone = (rule, value, callback) => {
  const patter = new RegExp('^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$')
  if (!patter.test(value)) {
    return callback(new Error('请输入正确格式的手机号！'))
  } else {
    callback() // 必须有此项回调，否则验证会一直不通过
  }
}
// 邮箱的正则
const validateEmail = (rule, value, callback) => {
  const mailbox = new RegExp('^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$')
  if (!mailbox.test(value)) {
    return callback(new Error('请输入正确格式的邮箱！'))
  } else {
    callback()
  }
}
// 身份证号
const validateIdCardNo = (rule, value, callback) => {
  // eslint-disable-next-line no-invalid-regexp
  const mailbox = new RegExp('^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}[0-9Xx]$')
  if (!mailbox.test(value)) {
    return callback(new Error('请输入正确格式的身份证号！'))
  } else {
    callback()
  }
}
// 网址校验
const validateWebsite = (rule, value, callback) => {
  const mailbox = new RegExp('^((https|http|ftp|rtsp|mms){0,1}(:\/\/){0,1})www\.(([A-Za-z0-9-~]+)\.)+([A-Za-z0-9-~\/])+$')
  if (!mailbox.test(value)) {
    return callback(new Error('请输入正确格式的网址！'))
  } else {
    callback()
  }
}
// 邮政编码校验
const validatePostalCode = (rule, value, callback) => {
  const mailbox = new RegExp('^(0[1234567]|1[012356]|2[01234567]|3[0123456]|4[01234567]|5[1234567]|6[1234567]|7[012345]|8[013456])\d{4}$')
  if (!mailbox.test(value)) {
    return callback(new Error('请输入正确格式的邮政编码！'))
  } else {
    callback()
  }
}
// 用户账号
const validateUsername = (rule, value, callback) => {
  const mailbox = new RegExp('^[a-zA-Z0-9_-]{3,16}$')
  if (!mailbox.test(value)) {
    return callback(new Error('3到16位（字母，数字，下划线，减号）！'))
  } else {
    callback()
  }
}
// 用户密码校验
const validatePassword = (rule, value, callback) => {
  const mailbox = new RegExp('^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$')
  if (!mailbox.test(value)) {
    return callback(new Error('8-16位字母数字组合！'))
  } else {
    callback()
  }
}
export default {
  common: {
    // ---------------------公共验证方法
    phone: [{ // 手机号
      required: true,
      message: '手机号不能为空'
    }, {
      validator: validatePhone,
      trigger: 'blur'
    }],
    email: [{ // 邮箱
        required: true,
        message: '邮箱不能为空'
      },
      {
        validator: validateEmail,
        trigger: 'blur'
      }
    ],
    idCardNo: [{ // 身份证号码
        required: true,
        message: '邮箱不能为空'
      },
      {
        validator: validateIdCardNo,
        trigger: 'blur'
      }
    ],
    website: [{ // 网址校验
        required: true,
        message: '邮箱不能为空'
      },
      {
        validator: validateWebsite,
        trigger: 'blur'
      }
    ],
    postalCode: [{ // 邮政编码校验
        required: true,
        message: '邮箱不能为空'
      },
      {
        validator: validatePostalCode,
        trigger: 'blur'
      }
    ],
  },
  login: {
    username: [{ // 用户账号校验
        required: true,
        message: '用户账号不能为空'
      },
      {
        validator: validateUsername,
        trigger: 'blur'
      }
    ],
    password: [{ // 用户密码校验
      required: true,
      message: '用户密码不能为空'
    },
    {
      validator: validatePassword,
      trigger: 'blur'
    }
  ],
  }
}